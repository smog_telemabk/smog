package com.example.smogcheck;

import android.location.Location;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class SmogItem {
    private String stationName;
    private Location stationCords;
    private String smogName;
    private String smogRepr;
    private int smogValue;
    public static HashMap<String, Location> addressMapper = new HashMap<String, Location>(){
        {
            Location location;

            location = new Location("Al. Krasińskiego");
            location.setLatitude(50.056829);
            location.setLongitude(19.926414);
            put("Al. Krasińskiego", location);

            location = new Location("ul. Bulwarowa");
            location.setLatitude(50.078371);
            location.setLongitude(20.050899);
            put("ul. Bulwarowa", location);

            location = new Location("ul. Bujaka");
            location.setLatitude(50.009870);
            location.setLongitude(19.951424);
            put("ul. Bujaka", location);

            location = new Location("ul. Dietla");
            location.setLatitude(50.054217);
            location.setLongitude(19.943289);
            put("ul. Dietla", location);

            location = new Location("os. Piastów");
            location.setLatitude(50.099606);
            location.setLongitude(20.016707);
            put("os. Piastów", location);

            location = new Location("ul. Złoty Róg");
            location.setLatitude(50.081699);
            location.setLongitude(19.895629);
            put("ul. Złoty Róg", location);
        }
    };



    public SmogItem (JSONObject smogObject){
        try {
            this.stationName = smogObject.getString("station_name");
            this.stationCords = addressMapper.get(this.stationName);
            this.smogRepr = smogObject.getJSONArray("details").getJSONObject(0).getString("g_nazwa");
            this.smogName = smogObject.getJSONArray("details").getJSONObject(0).getString("par_html");
            this.smogValue = smogObject.getJSONArray("details").getJSONObject(0).getInt("o_value");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static List<SmogItem> fromJSON(JSONArray jsonObjects){
        ArrayList<SmogItem> items = new ArrayList<>();
        for (int i = 0; i < jsonObjects.length(); i++) {
            try {
                items.add(new SmogItem(jsonObjects.getJSONObject(i)));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return items;
    }


    public String getStationName() {
        return stationName;
    }

    public String getSmogName() {
        return smogName;
    }

    public int getSmogValue() {
        return smogValue;
    }

    public Location getStationCords() {
        return stationCords;
    }

    public String getSmogRepr() {
        return smogRepr;
    }
}
